﻿using System;
using System.Collections.Generic;
using System.Text;
using CWk_9.Data;

namespace CWk_9.Tests
{
    class ContextGetter
    {
        private static ApplicationDbContext context;

        public static ApplicationDbContext GetApplicationContext()
        {
            if (context == null)
            {
                context = new ApplicationDbContext(@"Server=(localdb)\\mssqllocaldb;Database=aspnet-CWk_9Test-4261B901-A4B3-434D-AE6F-9E4B982F3D3C;Trusted_Connection=True;MultipleActiveResultSets=true");
            }

            return context;
        }
    }
}
