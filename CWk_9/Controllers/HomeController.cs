﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CWk_9.Data;
using Microsoft.AspNetCore.Mvc;
using CWk_9.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;

namespace CWk_9.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext context;
        private UserManager<ApplicationUser> _userManager;
        private readonly IStringLocalizer<HomeController> _localizer;

        public HomeController(
            ApplicationDbContext context,
            UserManager<ApplicationUser> _userManager,
        IStringLocalizer<HomeController> _localizer
            )
        {
            this.context = context;
            this._userManager = _userManager;
            this._localizer = _localizer;
        }
        public IActionResult Index()
        {
           // ApplicationUser user = context.Users.FirstOrDefault(u => u.Email == HttpContext.User.Identity.Name);
           
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
