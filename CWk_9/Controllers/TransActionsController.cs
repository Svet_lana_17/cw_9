﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CWk_9.Data;
using CWk_9.Models;
using CWk_9.Models.TransActionsViewModel;
using Microsoft.Extensions.Localization;

namespace CWk_9.Controllers
{
    public class TransActionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IStringLocalizer<HomeController> _localizer;

        public TransActionsController(
            ApplicationDbContext context,
            IStringLocalizer<HomeController> _localizer)
        {
            _context = context;
            this._localizer = _localizer;
        }

        // GET: TransActions
        public async Task<IActionResult> Index()
        {
            return View(await _context.TransActions.ToListAsync());
        }

        // GET: TransActions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transAction = await _context.TransActions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (transAction == null)
            {
                return NotFound();
            }

            return View(transAction);
        }

        // GET: TransActions/Create
        public IActionResult AddBalance( /*int pers_acc, double sum*/) //надо ли сюда что-то принимать
        {
            return View();
        }

        // POST: TransActions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBalance(AddBalanceViewModel model)
        {
            ApplicationUser user = _context.Users.FirstOrDefault(u => u.Personal_Account == model.Pers_Acc);
            //if (_context.Users.Contains(user))
            if (user != null)
            {
                user.Balance = user.Balance + model.Sum;

                _context.Update(user);
                _context.SaveChanges();
                return RedirectToAction("AddBalanceSuccess");
            }

            return RedirectToAction("Error");

            return View();
        }

        public async Task<IActionResult> TransAct()
        {
            return View();
        }

        [HttpPost]
        // GET: TransActions/Edit/5
        public async Task<IActionResult> TransAct(TransActionViewModel model)
        {
            ApplicationUser senduser = _context.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            ApplicationUser accept_user =
                _context.Users.FirstOrDefault(u => u.Personal_Account == model.AcceptUser_Pers_Account);
            if (senduser.Balance >= model.Sum)
            {
                TransAction transAction = new TransAction()
                {
                    Sum = model.Sum,
                    AcceptUser_Pers_Account = model.AcceptUser_Pers_Account,
                    //User1 = senduser,
                    SendUserId = senduser.Id,
                    // User = accept_user,//_context.Users.FirstOrDefault(u => u.Personal_Account == model.AcceptUser_Pers_Account),
                    DateOfTransaction = DateTime.Now
                };
                senduser.Balance = senduser.Balance - model.Sum;
                //ApplicationUser accept_user = _context.Users.FirstOrDefault(u => u.Personal_Account == model.AcceptUser_Pers_Account);
                accept_user.Balance = accept_user.Balance + model.Sum;
                //transAction.User.Balance = transAction.User.Balance + model.Sum;
                _context.Add(transAction);
                _context.SaveChanges();
                return View("TransAct_Success", transAction);
            }

            return View("TransAct_Error");

            return View();
        }

        // POST: TransActions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Sum,UserId")] TransAction transAction)
        {
            if (id != transAction.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(transAction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TransActionExists(transAction.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            return View(transAction);
        }

        // GET: TransActions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var transAction = await _context.TransActions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (transAction == null)
            {
                return NotFound();
            }

            return View(transAction);
        }

        // POST: TransActions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var transAction = await _context.TransActions.SingleOrDefaultAsync(m => m.Id == id);
            _context.TransActions.Remove(transAction);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TransActionExists(int id)
        {
            return _context.TransActions.Any(e => e.Id == id);
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult AddBalanceSuccess()
        {
            return View();
        }

        public IActionResult Pay_for_services(Pay_for_servicesViewModel model)
        {
            return View();
        }

        [HttpPost]
        public IActionResult Pay_for_services(int id, double sum)
        {
            ApplicationUser user = _context.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            if (user.Balance >= sum)
            {
                user.Balance = user.Balance - sum;
                _context.Update(user);
                _context.SaveChanges();
                return View("TransAct_Success");
                
            }
            return View("TransAct_Error");
        }

        //return View();
        }

    
}

