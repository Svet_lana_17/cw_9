﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CWk_9.Migrations
{
    public partial class AddFieldTransact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TransActionId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_TransActionId",
                table: "AspNetUsers",
                column: "TransActionId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_TransActions_TransActionId",
                table: "AspNetUsers",
                column: "TransActionId",
                principalTable: "TransActions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_TransActions_TransActionId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_TransActionId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TransActionId",
                table: "AspNetUsers");
        }
    }
}
