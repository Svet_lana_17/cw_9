﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace CWk_9.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        [Display(Name = "Лицевой счет")]
        public int Personal_Account { get; set; }

       // [Display(Name = "Баланс")]
        public double Balance { get; set; }

        public int TransAction_Count { get; set; }

        //public TransAction TransAction { get; set; }
        public List<TransAction> TransActions { get; set; }

        public ApplicationUser()
        {
            TransActions = new List<TransAction>();
        }
    }
}
