﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CWk_9.Models.AccountViewModels
{
    public class LoginViewModel
    {
        //[Required]
        [EmailAddress]
        public string Email { get; set; }
        public int Personal_Account { get; set; }
        //public ApplicationUser User { get; set; }

        // [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
