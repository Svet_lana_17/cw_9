﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CWk_9.Models
{
    public class TransAction
    {
        public int Id { get; set; }
        [Display(Name = "Сумма")]
        public double Sum { get; set; }

        [Display(Name = "от кого")]
        public string SendUserId { get; set; }
        //public ApplicationUser User1 { get; set; }

        [Display(Name = "Лицевой счет получателя")]
        public int AcceptUser_Pers_Account { get; set; }
        //public ApplicationUser User { get; set; }

        [Display(Name = "Дата и время перевода")]
        public DateTime DateOfTransaction { get; set; }
    }
}
