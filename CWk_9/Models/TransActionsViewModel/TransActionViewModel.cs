﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CWk_9.Models.TransActionsViewModel
{
    public class TransActionViewModel
    {
        [Display(Name = "Сумма")]
        public double Sum { get; set; }
        [Display(Name = "Лицевой счет получателя")]
        public int AcceptUser_Pers_Account { get; set; }
    }
}
