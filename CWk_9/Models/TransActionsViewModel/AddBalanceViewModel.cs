﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CWk_9.Models.TransActionsViewModel
{
    public class AddBalanceViewModel
    {
        public int Pers_Acc { get; set; }
        public double Sum { get; set; }
    }
}
