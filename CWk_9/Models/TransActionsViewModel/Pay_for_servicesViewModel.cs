﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CWk_9.Models.TransActionsViewModel
{
    public class Pay_for_servicesViewModel
    {
        public string NameOfService { get; set; }
        public double Sum { get; set; }
    }
}
